<?php
// On démarre la session AVANT d'écrire du code HTML
session_start();

// On s'amuse à créer quelques variables de session dans $_SESSION
$_SESSION['prenom'] = 'Billy';
$_SESSION['nom'] = 'Dupont';
$_SESSION['age'] = 23;
?>

<?php setcookie('pseudo', 'Billy29', time() + 365*24*3600, null, null, false, true); ?>



<?php

// Le mot de passe n'a pas été envoyé ou n'est pas bon
if (!isset($_POST['motdepasse']) OR $_POST['motdepasse'] != "azerty")
{
	// Afficher le formulaire de saisie du mot de passe
	echo '<p>Mot de passe correct</p>';
}
// Le mot de passe a été envoyé et il est bon
else
{
	// Afficher les codes secrets
	echo '<p>Mot de passe incorrect</p>';
}

?>